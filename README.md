## For start on prod server
pm2 start ./ecosystem.config.js

## For restart on prod server
pm2 restart ./ecosystem.config.js

## For stop on prod server
pm2 stop ./ecosystem.config.js

## For look logs
pm2 logs --lines 20

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```