import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Param,
  Delete,
  UseGuards,
  ParseIntPipe,
  Patch,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

import { UserService } from '../../shared/services/user/user.service';
import { UserEntity } from '../../shared/entitys/user.entity';
import { UserData } from '../../shared/decorators/user.decorator';

import { CreateUserDTO } from '../../shared/dto/create-user.dto';
import { AddQueueDTO } from '../../shared/dto/queue-add.dto';

@Controller('user')
@UseGuards(AuthGuard('jwt'))
export class UserController {

  constructor(
    private userService: UserService,
  ) {
  }

  @Get()
  getUser(
    @UserData() user: UserEntity,
  ): Promise<UserEntity> {
    return this.userService.findOne(user.id);
  }

  @Put()
  update(
    @Body() userData: UserEntity,
    @UserData() user: UserEntity,
  ): Promise<any> {
    userData.id = Number(user.id);
    return this.userService.update(userData);
  }

  @Get('queue')
  getAllUserQueue(
    @UserData() user: UserEntity,
  ): Promise<any> {
    return this.userService.getAllUserQueues(user);
  }

  @Patch('queue/add')
  addToQueue(
    @Body() data: AddQueueDTO,
    @UserData() user: UserEntity,
  ): Promise<any> {
    return this.userService.addToQueue(data, user);
  }

  @Patch('queue/remove')
  removeFromQueue(
    @Body() data: AddQueueDTO,
    @UserData() user: UserEntity,
  ): Promise<any> {
    return this.userService.removeFromQueue(data, user);
  }

  // TODO: close this rout for all users
  // This rout only for admin
  @Get('all')
  getUsers(): Promise<UserEntity[]> {
    return this.userService.findAll();
  }

  // TODO: close this rout for all users
  // This rout only for admin
  @Get(':id')
  getUserById(@Param('id', ParseIntPipe) id: number): Promise<UserEntity> {
    return this.userService.findOne(id);
  }

  // TODO: close this rout for all users
  // This rout only for admin
  @Post()
  create(@Body() userData: CreateUserDTO): Promise<any> {
    return this.userService.create(userData);
  }

  // TODO: close this rout for all users
  // This rout only for admin
  @Put(':id')
  updateById(
    @Param('id', ParseIntPipe) id: number,
    @Body() userData: UserEntity,
  ): Promise<any> {
    userData.id = Number(id);
    return this.userService.update(userData);
  }

  // TODO: close this rout for all users
  // This rout only for admin
  @Delete(':id')
  delete(@Param('id', ParseIntPipe) id: number): Promise<any> {
    return this.userService.delete(id);
  }
}
