import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { UserController } from './user/user.controller';
import { FileController } from './file/file.controller';

import { UserService } from '../shared/services/user/user.service';
import { AudioFilesService } from '../shared/services/audio-files/audio-files.service';

import { UserEntity } from '../shared/entitys/user.entity';
import { AudioFileEntity } from '../shared/entitys/audio-files.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      UserEntity,
      AudioFileEntity,
    ]),
  ],
  controllers: [
    UserController,
    FileController,
  ],
  providers: [
    UserService,
    AudioFilesService,
  ],
})
export class UserModule {}
