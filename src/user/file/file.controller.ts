import {
  Controller,
  Get,
  Post,
  Param,
  UseGuards,
  UseInterceptors,
  UploadedFile,
  Res,
  Delete,
  Query,
  ParseIntPipe,
} from '@nestjs/common';

import { FileInterceptor } from '@nestjs/platform-express';
import { AuthGuard } from '@nestjs/passport';
import { diskStorage } from  'multer';
import { extname } from  'path';

import { AudioFilesService } from '../../shared/services/audio-files/audio-files.service';
import { AudioFileEntity } from '../../shared/entitys/audio-files.entity';

import { SearchAudioDTO } from '../../shared/dto/search-audio.dto';
import { UploadAudioFileDTO } from '../../shared/dto/upload-audio-file.dto';

@Controller('file')
@UseGuards(AuthGuard('jwt'))
export class FileController {

  constructor(
    private audioFilesService: AudioFilesService,
  ) {
  }

  @Get()
  getAll(@Query() filter: SearchAudioDTO) {
    return this.audioFilesService.findAll(filter);
  }

  @Get(':id')
  async getFileById(
    @Param('id', ParseIntPipe) id: number,
    @Res() res,
  ) {
    const file: AudioFileEntity = await this.audioFilesService.findOne(id);

    if (!file) {
      throw new Error('File not found');
    }

    res.sendFile(file.filename, {
      root: file.destination,
    });
  }

  @Get(':id/info')
  async getFileInfoById(@Param('id', ParseIntPipe) id: number) {
    const file: AudioFileEntity = await this.audioFilesService.findOne(id);

    if (!file) {
      throw new Error('File not found');
    }

    return file;
  }

  @Post()
  @UseInterceptors(FileInterceptor('file', {
    storage: diskStorage({
      destination: './uploads/audio',
      filename: (req, file, cb) => {
        cb(null, `${Date.now()}${extname(file.originalname)}`);
      },
    }),
    fileFilter: (req, file, cb) => {
      if (!extname(file.originalname).match(/\.(mp3|ogg)$/)) {
        cb(new Error('Only audio files are allowed!'), false);
      } else {
        cb(null, true);
      }
    },
  }))
  async uploadFile(@UploadedFile() file: UploadAudioFileDTO) {
    const fileData = {
      originalname: file.originalname.split('.')[0],
      destination: file.destination,
      filename: file.filename,
      author: '',
      imageUrl: '',
    };

    try {
      return await this.audioFilesService.create(fileData);
    } catch (error) {
      await this.audioFilesService.deleteFileByName(file.filename);

      throw new Error(`File ${file.filename} is exist. ${error}`);
    }
  }

  // TODO: close this rout for all users
  // This rout only for admin
  @Delete()
  async deleteAll(@Res() res) {
    try {
      await this.audioFilesService.deleteAll();
      await this.audioFilesService.deleteAllFiles();

      res.status(200).json({
        message: 'All files was removed',
      });
    } catch (error) {
      throw new Error(`Files were not removed: ${error}`);
    }
  }

  // TODO: close this rout for all users
  // This rout only for admin
  @Delete(':id')
  async deleteById(@Param('id', ParseIntPipe) id: number) {
    try {
      return await this.audioFilesService.delete(id);
    } catch (error) {
      throw new Error(`File was not removed: ${error}`);
    }
  }
}
