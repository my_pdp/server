import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';
import * as config from 'config';
import * as io from 'socket.io';
import * as fileSystem from 'fs';
import { SocketListenersService } from './shared/services/socket/socket-listeners/socket-listeners.service';

const generalConfig = config.get('GENERAL');

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.enableCors({
    origin: '*',
  });

  app.useGlobalPipes(new ValidationPipe());

  app.setGlobalPrefix('api');

  await app.listen(generalConfig.PORT);
  console.log(`Server run in ${generalConfig.PORT} port.`, new Date());

  socket();
}
bootstrap();

async function socket() {
  const socketListenersService = new SocketListenersService();

  const ioConnect = await io.listen(generalConfig.SOCKET_PORT);
  console.log(`Socket run in ${generalConfig.SOCKET_PORT} port.`, new Date());

  ioConnect.on('connection', (socket) => {
    console.info(`Socket client connected [id=${socket.id}]`, new Date());

    let isReading = false;
    let readStream;
    let index = 0;

    socket.on('get-file', (audio) => {

      if (isReading) {
        readStream.close();
        isReading = false;
      }

      const filePath = `${audio.destination}/${audio.filename}`;

      readStream = fileSystem.createReadStream(filePath, {
        highWaterMark: 100 * 1024,
      });

      isReading = true;

      const data = {
        audio,
        filedata: fileSystem.statSync(filePath),
      };

      readStream.on('open', () => {
        index = 0;
        socketListenersService.emitAudioStart(socket, data);
      });

      readStream.on('data', (data) => {
        index += 1;
        const temp = {
          data,
          index,
        };

        socketListenersService.emitChunk(socket, temp);
      });

      readStream.on('end', () => {
        isReading = false;
        socketListenersService.emitAudioEnd(socket);
      });
    });

  });
}
