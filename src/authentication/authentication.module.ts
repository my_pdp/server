import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';

import { LoginController } from './login/login.controller';
import { RegisterController } from './register/register.controller';
import { RestoreController } from './restore/restore.controller';

import { UserService } from '../shared/services/user/user.service';
import { AuthService } from './shared/services/auth/auth.service';
import { JwtConfigService } from '../shared/services/jwt-config/jwt-config.service';

import { LocalStrategy } from './shared/local.strategy';
import { JwtStrategy } from './shared/jwt.strategy';

import { UserEntity } from '../shared/entitys/user.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([UserEntity]),
    PassportModule,
    JwtModule.registerAsync({
      useClass: JwtConfigService,
    }),
  ],
  controllers: [
    LoginController,
    RegisterController,
    RestoreController,
  ],
  providers: [
    UserService,
    AuthService,
    LocalStrategy,
    JwtStrategy,
  ],
})
export class AuthenticationModule {}
