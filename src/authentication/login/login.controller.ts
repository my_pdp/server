import { Controller, Post, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

import { UserData } from '../../shared/decorators/user.decorator';
import { AuthService } from '../shared/services/auth/auth.service';

@Controller('login')
export class LoginController {

  constructor(
    private authService: AuthService,
  ) {
  }

  @UseGuards(AuthGuard('local'))
  @Post()
  login(@UserData() user) {
    return this.authService.login(user);
  }
}
