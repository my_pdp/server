import { Controller, Post, Body } from '@nestjs/common';
import { AuthService } from '../shared/services/auth/auth.service';
import { CreateUserDTO } from '../../shared/dto/create-user.dto';

@Controller('register')
export class RegisterController {

  constructor(
    private authService: AuthService,
  ) {
  }

  @Post()
  register(@Body() user: CreateUserDTO): Promise<any> {
    return this.authService.register(user);
  }
}
