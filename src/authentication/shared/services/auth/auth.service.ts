import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcryptjs';

import { UserService } from '../../../../shared/services/user/user.service';
import { UserEntity } from '../../../../shared/entitys/user.entity';
import { CreateUserDTO } from '../../../../shared/dto/create-user.dto';

@Injectable()
export class AuthService {
  constructor(
    private jwtService: JwtService,
    private userService: UserService,
  ) {
  }

  /**
   * Register user and return token
   * @param user
   */
  async register(user: CreateUserDTO): Promise<any> {
    const newUser = await this.userService.create(user);

    if (!newUser) {
      throw new UnauthorizedException();
    }

    const payload = {
      email: newUser.email,
      sub: newUser.id,
    };

    return {
      access_token: this.jwtService.sign(payload),
    };
  }

  /**
   * Check if user exits and check is this password correct
   * @param userData
   */
  public async validateUser(email: string, password: string): Promise<UserEntity> {
    const user = await this.userService.findByEmail(email);

    if (user) {
      const match = await bcrypt.compare(password, user.password);

      if (match) {
        return user;
      }
    }

    return null;
  }

  /**
   * Login user and return token
   * @param user
   */
  async login(user: any) {
    const payload = {
      email: user.email,
      sub: user.id,
    };

    return {
      access_token: this.jwtService.sign(payload),
    };
  }

}
