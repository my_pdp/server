import { Test, TestingModule } from '@nestjs/testing';
import { SocketListenersService } from './socket-listeners.service';

describe('SocketListenersService', () => {
  let service: SocketListenersService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SocketListenersService],
    }).compile();

    service = module.get<SocketListenersService>(SocketListenersService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
