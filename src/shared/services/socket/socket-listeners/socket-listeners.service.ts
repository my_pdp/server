import { Injectable } from '@nestjs/common';

@Injectable()
export class SocketListenersService {

  constructor(
  ) { }

  emitAudioStart(socket, data) {
    socket.emit('audio-start', data);
  }

  emitAudioEnd(socket) {
    socket.emit('audio-end', true);
  }

  emitChunk(socket, data) {
    socket.emit('audio-chunk', data);
  }
}
