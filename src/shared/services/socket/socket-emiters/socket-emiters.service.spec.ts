import { Test, TestingModule } from '@nestjs/testing';
import { SocketEmitersService } from './socket-emiters.service';

describe('SocketEmitersService', () => {
  let service: SocketEmitersService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SocketEmitersService],
    }).compile();

    service = module.get<SocketEmitersService>(SocketEmitersService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
