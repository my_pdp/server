import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, DeleteResult, UpdateResult } from 'typeorm';
import * as bcrypt from 'bcryptjs';

import { UserEntity } from '../../entitys/user.entity';
import { CreateUserDTO } from 'src/shared/dto/create-user.dto';

@Injectable()
export class UserService {

  constructor(
    @InjectRepository(UserEntity)
    private userRepository: Repository<UserEntity>,
  ) { }

  findAll(): Promise<UserEntity[]> {
    return this.userRepository.find();
  }

  async findOne(id: number): Promise<UserEntity> {
    const user = await this.userRepository.findOne(id);

    if (!user) {
      throw new NotFoundException(`User with ID ${id} not found.`);
    }

    return user;
  }

  async create(user: CreateUserDTO): Promise<UserEntity> {
    // hash password
    // @TODO: use solt generator for improve security
    user.password = await bcrypt.hash(user.password, 10);

    return this.userRepository.save(user);
  }

  update(user: UserEntity): Promise<UpdateResult> {
    return this.userRepository.update(user.id, user);
  }

  addToQueue(
    data: {
      id: number[],
    },
    user: UserEntity,
  ): Promise<any> {
    return this.userRepository
      .createQueryBuilder()
      .relation(UserEntity, 'queue')
      .of(user.id)
      .add(data.id);
  }

  removeFromQueue(
    data: {
      id: number[],
    },
    user: UserEntity,
  ): Promise<any> {
    return this.userRepository
      .createQueryBuilder()
      .relation(UserEntity, 'queue')
      .of(user.id)
      .remove(data.id);
  }

  getAllUserQueues(user: UserEntity): Promise<any> {
    return this.userRepository
      .createQueryBuilder()
      .relation(UserEntity, 'queue')
      .of(user.id)
      .loadMany();
  }

  delete(id: number): Promise<DeleteResult> {
    return this.userRepository.delete(id);
  }

  findByEmail(email: string): Promise<UserEntity> {
    return this.userRepository.findOne({
      where: {
        email,
      },
    });
  }

  findById(id: number): Promise<UserEntity> {
    return this.userRepository.findOne({
      where: {
        id,
      },
    });
  }
}
