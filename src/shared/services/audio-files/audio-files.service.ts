import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, DeleteResult } from 'typeorm';

import { AudioFileEntity } from '../../../shared/entitys/audio-files.entity';
import { SearchAudioDTO } from '../../dto/search-audio.dto';

import * as rimraf from  'rimraf';

@Injectable()
export class AudioFilesService {
  constructor(
    @InjectRepository(AudioFileEntity)
    private audioFilesRepository: Repository<AudioFileEntity>,
  ) { }

  findAll(
    filter: SearchAudioDTO): Promise<AudioFileEntity[]> {
    const { search } = filter;
    const query = this.audioFilesRepository.createQueryBuilder('audio');

    // @TODO: add search with ignoreCase
    if (search) {
      query.andWhere('audio.originalname LIKE :search', {
        search: `%${search}%`,
      });
    }

    return query.getMany();
  }

  findOne(id): Promise<AudioFileEntity> {
    return this.audioFilesRepository.findOne(id);
  }

  create(fileData): Promise<AudioFileEntity> {
    return this.audioFilesRepository.save(fileData);
  }

  async delete(id): Promise<any> {
    const file: AudioFileEntity = await this.audioFilesRepository.findOne(id);

    if (file) {
      return await Promise.all([
        this.deleteFileByName(file.filename),
        this.audioFilesRepository.delete(id),
      ]);
    }

    throw new Error('File not found');
  }

  deleteFileByName(filename: string) {
    return new Promise((resolve, reject) => {
      return rimraf(`./uploads/audio/${filename}`, (result) => {
        resolve(result);
      });
    });
  }

  deleteAllFiles() {
    return new Promise((resolve, reject) => {
      return rimraf('./uploads/audio/*', (result) => {
        resolve(result);
      });
    });
  }

  deleteAll(): Promise<any> {
    return this.audioFilesRepository
      .createQueryBuilder()
      .delete()
      .from(AudioFileEntity)
      .execute();
  }
}
