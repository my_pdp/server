import { Injectable } from '@nestjs/common';
import { JwtOptionsFactory, JwtModuleOptions } from '@nestjs/jwt';
import * as config from 'config';

const jwtConfig = config.get('JWT');

@Injectable()
export class JwtConfigService implements JwtOptionsFactory {
  createJwtOptions(): JwtModuleOptions {
    return {
      secret: jwtConfig.JWT_SECRET,
      signOptions: {
        expiresIn: jwtConfig.JWT_EXPIRES,
      },
    };
  }
}
