import {
  ExceptionFilter,
  Catch,
  ArgumentsHost,
  HttpStatus,
} from '@nestjs/common';

import {
  Request,
  Response,
} from 'express';

@Catch()
export class AllExceptionsFilter implements ExceptionFilter {
  catch(exception: any, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    const request = ctx.getRequest<Request>();

    const defaultError = {
      message: exception.message,
      name: exception.name,
      code: exception.code,
    };

    const error = exception.getResponse
      ? exception.getResponse()
      : defaultError;

    const status = exception.getStatus
      ? exception.getStatus()
      : HttpStatus.INTERNAL_SERVER_ERROR;

    const message = status !== HttpStatus.INTERNAL_SERVER_ERROR
      ? exception.message.error || exception.message || null
      : 'Internal server error';

    const errorResponse = {
      error,
      message,
      timestamp: new Date().toLocaleDateString(),
      path: request.url,
    };

    response
      .status(status)
      .json(errorResponse);
  }
}
