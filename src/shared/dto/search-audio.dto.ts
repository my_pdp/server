import {
  IsNotEmpty,
  IsOptional,
} from 'class-validator';

export class SearchAudioDTO {
  @IsOptional()
  @IsNotEmpty()
  search: string;
}
