import {
  IsNumber,
  IsArray,
  ArrayNotEmpty,
} from 'class-validator';

export class AddQueueDTO {
  @ArrayNotEmpty()
  @IsArray()
  @IsNumber(null, {
    each: true,
  })
  id: number[];
}
