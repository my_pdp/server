import {
  IsNotEmpty,
} from 'class-validator';

export class UploadAudioFileDTO {
  @IsNotEmpty()
  originalname: string;

  @IsNotEmpty()
  destination: string;

  @IsNotEmpty()
  filename: string;
}
