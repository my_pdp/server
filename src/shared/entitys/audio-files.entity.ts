import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  Index,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToMany,
} from 'typeorm';

import { UserEntity } from './user.entity';

@Entity()
class AudioFile {
  @PrimaryGeneratedColumn()
  id: number;

  @Index({
    unique: true,
  })
  @Column()
  originalname: string;

  @Column()
  destination: string;

  @Column()
  author: string;

  @Column()
  imageUrl: string;

  @Column()
  filename: string;

  @ManyToMany(type => UserEntity, user => user.queue)
  users: UserEntity[];

  @UpdateDateColumn()
  updated_at: string;

  @CreateDateColumn()
  created_at: string;
}

export { AudioFile as AudioFileEntity };
