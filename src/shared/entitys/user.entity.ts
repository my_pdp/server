import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  Index,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToMany,
  JoinTable,
} from 'typeorm';

import { AudioFileEntity } from './audio-files.entity';

@Entity()
class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Index({
    unique: true,
  })
  @Column()
  email: string;

  @Column()
  password: string;

  @ManyToMany(type => AudioFileEntity, audioFile => audioFile.users)
  @JoinTable()
  queue: AudioFileEntity[];

  @UpdateDateColumn()
  updated_at: string;

  @CreateDateColumn()
  created_at: string;
}

export { User as UserEntity };
