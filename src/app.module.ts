import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { APP_FILTER } from '@nestjs/core';

import { AllExceptionsFilter } from './shared/filters/http-exception.filter';

import { AuthenticationModule } from './authentication/authentication.module';
import { UserModule } from './user/user.module';

import { TypeOrmConfigService } from './shared/services/type-orm-config/type-orm-config.service';

@Module({
  imports: [
    AuthenticationModule,
    UserModule,
    TypeOrmModule.forRootAsync({
      useClass: TypeOrmConfigService,
    }),
  ],
  controllers: [
  ],
  providers: [
    {
      provide: APP_FILTER,
      useClass: AllExceptionsFilter,
    },
  ],
})
export class AppModule {
}
